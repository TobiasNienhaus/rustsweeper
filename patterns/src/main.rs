enum Tern {
    One(i32),
    Two(i32, i32),
    N(bool),
    Other,
}

enum B {
    A, B, C,
}

#[derive(Debug)]
struct S {
    v: i32
}

impl S {
    fn flip(&mut self) {
        *self = S {v: -self.v};
    }

    fn s(v: i32) -> S {
        S { v }
    }
}

fn main() {
    let mut s = S::s(5);
    println!("Before flip: {:?}", s);
    s.flip();
    println!("After flip: {:?}", s);

    println!("---------------------");

    let v = vec![
        Tern::One(1),
        Tern::Two(2, 3),
        Tern::N(false),
        Tern::Other,
        Tern::One(99),
    ];
    for t in v.iter() {
        match t {
            Tern::One(i) => println!("One: {}", i),
            Tern::Two(i, j) => println!("Two: {}, {}", i, j),
            Tern::N(b) => println!("None? {}", b),
            _ => println!("Unknown"),
        }
    }

    println!("---------------------");

    for t in v.iter() {
        let x = match t {
            Tern::One(i) => *i,
            Tern::Two(i, j) => *i * *j,
            Tern::N(b) => if *b { 1 } else { 0 },
            _ => i32::MIN,
        };

        match t {
            Tern::One(1) => println!("ONE!!!"),
            Tern::One(x) if x % 2 != 0 => println!("Not divisible by two"),
            Tern::One(i) => println!("One: {}", i),
            Tern::Two(i, j) => println!("Two: {}, {}", i, j),
            Tern::N(b) => println!("None? {}", b),
            _ => (),
        }
        println!("Result is: {}", x);
    }

    println!("---------------------");

    let mut o = Some(4);
    let x = if let Some(x) = o.take(){ x } else {0};
    let y = o.unwrap_or_default();
    println!("x: {}; y: {}", x, y);

    println!("---------------------");

    let (x, _, y, _, z) = (1, 2, 3, 4, 5);
    println!("x: {}, y: {}, z: {}", x, y, z);

    println!("---------------------");

    let mut setting_value: Option<i32> = None;
    let new_setting_value = None;

    match (setting_value, new_setting_value) {
        (_, None) => {
            println!("Can't delete setting");
        },
        (Some(_), Some(_)) => {
            println!("Can't overwrite an existing customized value");
        }
        _ => {
            setting_value = new_setting_value;
        }
    }

    println!("setting is {:?}", setting_value);

    println!("---------------------");

    struct Point {
        x: i32,
        _y: i32,
        z: i32,
    }

    let origin = Point { x: 0, _y: 0, z: 0 };

    match origin {
        Point { x, z, .. } => println!("x is {}; z is {}", x, z),
    }

    println!("---------------------");

    let b = B::A;
    let x = match b {
        B::A => 0,
        B::B => 1,
        B::C => 2,
    };
    println!("Some num: {}", x);

    println!("---------------------");

    let num = Some(4);

    match num {
        Some(x) if x < 5 => println!("less than five: {}", x),
        Some(x) => println!("{}", x),
        None => (),
    }

    println!("---------------------");

    let x = 4;
    let y = false;

    match x {
        4 | 5 | 6 if y => println!("yes"),
        _ => println!("no"),
    }

    println!("---------------------");

    enum Message {
        Hello { id: i32 },
    }

    let msg = Message::Hello { id: 5 };

    match msg {
        Message::Hello {
            id: id_variable @ 3..=7,
        } => println!("Found an id in range: {}", id_variable),
        Message::Hello { id: 10..=12 } => {
            println!("Found an id in another range")
        }
        Message::Hello { id } => println!("Found some other id: {}", id),
    }

    println!("---------------------");

    let x = Some(5);

    match x {
        Some(x @ 2..=5) => println!("{} is 2-5", x),
        Some(x) => println!("{} is NOT 2-5", x),
        _ => println!("Don't know"),
    }

    println!("---------------------");

    color_stuff();
}

fn color_stuff() {
    let favorite_color: Option<&str> = None;
    let is_tuesday = false;
    let age: Result<u8, _> = "34".parse();

    if let Some(color) = favorite_color {
        println!("Using your favorite color, {}, as the background", color);
    } else if is_tuesday {
        println!("Tuesday is green day!");
    } else if let Ok(age) = age {
        if age > 30 {
            println!("Using purple as the background color");
        } else {
            println!("Using orange as the background color");
        }
    } else {
        println!("Using blue as the background color");
    }

    age.and_then(|num| {
        println!("Age is {}", num);
        Ok(num * 2)
    }).and_then(|num| {
        println!("Now age is {}", num);
        Ok(())
    }).expect("Hello");
}
