use rand::prelude::*;

struct It {}

impl Iterator for It {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        let mut rng = thread_rng();
        let num = rng.gen_range(10..1000);
        if num > 10 { Some(num) } else { None }
    }
}

/// Multiplies a number by 2.
/// 
/// This is just to test the documentation features
/// 
/// # Examples
/// 
/// ```
/// let arg = 5;
/// let answer = some_func(arg);
/// 
/// assert_eq!(arg * 2, answer);
/// ```
pub fn some_func(i: i64) -> i64 {
    i * 2
}

/// Adds one to the number given.
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = my_crate::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
pub fn add_one(x: i32) -> i32 {
    x + 1
}

fn main() {
    println!("Hello, world!");

    // let it = It {};
    // let str_map: Vec<_> = it.map(|num| num.to_string()).collect();
    // for val in str_map.iter() {
    //     print!("STRING: [");
    //     let mut counter = 0;
    //     for c in val.chars() {
    //         print!("{}: {},", counter, c);
    //         counter += 1;
    //     }
    //     println!("]");
    // }

    for num in 0..=20 {
        println!("Random num try: {}", num);
        let i = It {};
        let mut counter = 0;
        for val in i {
            println!("Some random number: {}", val);
            counter += 1;
        }
        println!("Got {} numbers from 0 to 9, where # != 0", counter);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn add_one_test() {
        assert_eq!(4, add_one(3));
    }
}
