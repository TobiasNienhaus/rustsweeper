use std::error::Error;
use std::thread;
use std::time::Duration;
use std::marker::PhantomData;
use std::collections::HashMap;
use std::hash::Hash;

struct Cacher<T, A, R>
where
    T: Fn(A) -> R,
    A: Eq + Hash + Copy,
    R: Copy,
{
    calculation: T,
    map: HashMap<A, R>,
    phantom: PhantomData<A>,
}

impl<T, A, R> Cacher<T, A, R>
where
    T: Fn(A) -> R,
    A: Eq + Hash + Copy,
    R: Copy,
{
    fn new(calculation: T) -> Cacher<T, A, R> {
        Cacher {
            calculation,
            map: HashMap::new(),
            phantom: PhantomData,
        }
    }

    fn value(&mut self, arg: A) -> R {
        match self.map.get(&arg) {
            Some(v) => *v,
            None => {
                let key = arg;
                let v = (self.calculation)(arg);
                self.map.insert(key, v);
                v
            }
        }
    }
}

fn expensive_fn(i: u64) -> u64 {
    println!("Calculating slowly: {}", i);
    thread::sleep(Duration::from_secs(2));
    i
}

pub fn run() -> Result<(), Box<dyn Error>> {
    let mut cacher = Cacher::new(|num| expensive_fn(num));
    println!("Using Cacher");
    println!("Res 1: {}", cacher.value(1));
    println!("Res 2: {}", cacher.value(2));
    println!("Res 1: {}", cacher.value(1));
    println!("Res 3: {}", cacher.value(3));
    println!("Res 1: {}", cacher.value(1));
    println!("Res 2: {}", cacher.value(2));
    println!("Res 3: {}", cacher.value(3));
    Ok(())
}
