use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

fn _deadlock() {
    let m1 = Arc::new(Mutex::new(String::from("m1")));
    let m2 = Arc::new(Mutex::new(String::from("m2")));
    let mut handles = vec![];
    {
        let m1 = Arc::clone(&m1);
        let m2 = Arc::clone(&m2);
        println!("Spawning thread #1");
        handles.push(thread::spawn(move || {
            let m1 = m1.lock().unwrap();
            println!("Acquired {}", m1);
            thread::sleep(Duration::from_millis(1000));
            let m2 = m2.lock().unwrap();
            println!("Acquired {}", m2);
        }));
    }
    {
        let m1 = Arc::clone(&m1);
        let m2 = Arc::clone(&m2);
        println!("Spawning thread #2");
        handles.push(thread::spawn(move || {
            let m2 = m2.lock().unwrap();
            println!("Acquired {}", m2);
            thread::sleep(Duration::from_millis(1000));
            let m1 = m1.lock().unwrap();
            println!("Acquired {}", m1);
        }));
    }
    // Is this naive?
    for h in handles {
        h.join().unwrap();
    }
}

fn main() {
    let mut handles = Vec::new();

    let m = Arc::new(Mutex::new(Vec::new()));

    for i in 1..50 {
        println!("Opening thread #{}", i);
        let m = Arc::clone(&m);
        handles.push(thread::spawn(move || {
            for j in 1..10 {
                println!("Waiting for mutex! (#{})", i);
                let mut v = m.lock().unwrap();
                println!("Got mutex! (#{}: {})", i, j);
                v.push(format!("#{}: {}", i, j));
            }
        }));
    }

    // Is this naive?
    for h in handles {
        h.join().unwrap();
    }

    let v = m.lock().unwrap();
    for s in v.iter() {
        println!("Got: \"{}\"", s);
    }

    // let (tx_thread, rx_main) = mpsc::channel();
    // let (tx_main, rx_thread) = mpsc::channel();

    // let handle = thread::spawn(move || {
    //     for i in 0..10 {
    //         thread::sleep(Duration::from_millis(1000));
    //         tx_thread.send(Some(i)).unwrap();
    //         let x = rx_thread.recv().unwrap();
    //         println!("Received {} in thread", x);
    //     }
    //     thread::sleep(Duration::from_millis(1000));
    //     tx_thread.send(None).unwrap();
    // });

    // while let Some(num) = rx_main.recv().unwrap() {
    //     println!("Received {} in main", num);
    //     thread::sleep(Duration::from_millis(1000));
    //     tx_main.send(num * 2).unwrap();
    //     println!("Waiting...");
    // }

    // println!("Received None");
    // handle.join().unwrap();
}
