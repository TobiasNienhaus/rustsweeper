use oop::gui::traits::Draw;
use oop::*;

type V = Vec<Box<dyn Draw>>;

fn main() {
    let v: V = vec![gui::Something::newb(12), gui::Else::newb(1.0)];
    for d in v {
        d.draw();
    }
    let mut o = Some(5);
    println!("o: {:?}", o);
    let x = o.take();
    println!("o: {:?}; x: {:?}", o, x);
    let d = gui::Something::new(12);
    d.draw();
    let d2 = gui::Else::new(1.0);
    d2.draw();

    println!("Hello, world!");
}
