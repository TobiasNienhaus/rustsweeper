pub mod traits;

pub struct Something {
    v: i32,
}

impl Something {
    pub fn new(v: i32) -> Something {
        Something { v }
    }

    pub fn newb(v: i32) -> Box<Something> {
        Box::new(Something::new(v))
    }
}

impl traits::Draw for Something {
    fn draw(&self) {
        println!("Something: {}", self.v);
    }
}

pub struct Else {
    v: f32,
}

impl Else {
    pub fn new(v: f32) -> Else {
        Else {v}
    }

    pub fn newb(v: f32) -> Box<Else> {
        Box::new(Else::new(v))
    }
}

impl traits::Draw for Else {
    fn draw(&self) {
        println!("Else: {}", self.v);
    }
}
