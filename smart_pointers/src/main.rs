use std::cell::RefCell;

struct Something {
    value: RefCell<Vec<i32>>,
}

impl Something {
    fn new() -> Something {
        Something {
            value: RefCell::new(vec![])
        }
    }
}

fn main() {
    let s = Something::new();
    let x = s.value.borrow();
    let b = s.value.borrow();
    println!("Len: {}", x.len());
}
