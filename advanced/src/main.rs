use std::{fmt::Display, ops::Add};

const A: i32 = 4;

const LONG: usize = 70;
const SHORT: usize = LONG - 5;

fn main() {
    unsafe_rust();
    adv_traits();
    adv_fn_closures();
    macros();
}

fn unsafe_rust() {
    seq(LONG);

    println!(" Using unsafe rust!");

    shy(SHORT);

    println!(" Dereference ptr:");
    let mut num = 5;
    let r1 = &num as *const i32;
    let r2 = &mut num as *mut i32;
    let null = 0x0usize as *const i32;
    println!("   {:>20}: {:?}", "r1 is", r1);
    println!("   {:>20}: {:?}", "r2 is", r2);
    println!("   {:>20}: {:?}", "null is", null);

    unsafe {
        println!("   {:>20}: {:?}", "*r1 is", *r1);
        println!("   {:>20}: {:?}", "*r2 is", *r2);
    }

    shy(SHORT);

    println!(" Getting the address of a const:");

    let r1 = &A as *const i32;
    println!("   {:>20}: {:?}", "const is", r1);
    unsafe {
        println!("   {:>20}: {:?}", "*const is", *r1);
    }

    println!();
}

fn adv_traits() {
    seq(LONG);

    println!(" Using advanced traits!");

    shy(SHORT);

    println!(" Associated types:");

    println!("   {:>20}: {}", "Type of I", I::name());
    println!("   {:>20}: {}", "Type of F", F::name());
    println!("   {:>20}: {}", "Type of String", String2::name());
    println!("   {:>20}: {}", "Type of U", U::name());

    shy(SHORT);

    println!(" Struct shenanigans");

    let t = Test {};
    println!("   {:>20}: {:?}", "t is", t);
    let c = Test::test();
    println!("   {:>20}: {:?}", "c is", c);

    println!("   {:>20}: {:?}", "Test2::test is", Test2::test());
    println!(
        "   {:>20}: {:?}",
        "fully qualified is",
        <Test2 as Intrusive>::test()
    );

    shy(SHORT);

    println!(" Newtype stuff");

    let v = vec![0, 1, 2, 3, 4, 5];
    println!("   {:>20}: {:?}", "v is", v);
    println!("   {:>20}: {}", "v wrapped is", Wrapper::wrap(v));

    println!();
}

fn adv_fn_closures() {
    seq(LONG);

    println!(" Advanced Functions and Closures!");

    shy(SHORT);

    println!(" Function pointers:");

    fn add_one(x: i32) -> i32 {
        x + 1
    }

    let r = add_one as *const fn(i32) -> i32;

    fn do_twice(f: fn(i32) -> i32, arg: i32) -> i32 {
        f(arg) + f(arg)
    }

    let r2 = do_twice as *const fn(i32) -> i32;
    println!("   {:>20}: {:?}", "fn address is", r);
    println!("   {:>20}: {:?}", "fn2 address is", r2);
    // TODO try to execute it
    // unsafe {
    //     println!("   {:>20}: {:?}", "fn(5) is", (*r)(5));
    // }
    println!("   {:>20}: {:?}", "fn2(fn, 5) is", do_twice(add_one, 5));

    shy(SHORT);

    println!(" Collecting to tupe structs");

    #[derive(Debug)]
    struct Value(u32);
    let v = vec![0, 1, 2];
    println!("   {:>20}: {:?}", "v is", v);
    let v2: Vec<Value> = v.into_iter().map(Value).collect();
    println!("   {:>20}: {:?}", "v2 is", v2);

    println!();
}


trait Name {
    type Type;

    fn name() -> &'static str {
        std::any::type_name::<Self::Type>()
    }
}

struct I {}
impl Name for I {
    type Type = i32;
}

struct F {}
impl Name for F {
    type Type = f32;
}

struct String2 {}
impl Name for String2 {
    type Type = String2;
}

struct U {}
impl Name for U {
    type Type = u128;
}

#[derive(Debug)]
struct Test;
impl Test {
    fn test() -> i32 {
        5
    }
}

struct Test2;

trait Intrusive {
    fn test() -> i32;
}

impl Intrusive for Test2 {
    fn test() -> i32 {
        4
    }
}

impl Test2 {
    fn test() -> i32 {
        5
    }
}

struct Wrapper<T> {
    wrapped: T,
}

impl<T> Wrapper<T> {
    fn wrap(t: T) -> Wrapper<T> {
        Wrapper { wrapped: t }
    }
}

impl<T> Display for Wrapper<Vec<T>>
where
    T: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut s = String::from("");
        if self.wrapped.len() > 1 {
            for i in 0..self.wrapped.len() - 1 {
                s = s.add(format!("{}, ", self.wrapped[i]).as_str());
            }
        }
        if let Some(a) = self.wrapped.last() {
            s = s.add(a.to_string().as_str());
        }
        write!(f, "{}", s)
    }
}

fn macros() {
    seq(LONG);

    println!(" MACROS!");

    shy(SHORT);

    println!(" Cheap vec! macro");

    #[macro_export]
    macro_rules! vec2 {
        ( $( $x:expr ),* ) => {
            {
                let mut temp_vec = Vec::new();
                $(
                    println!("   {:>20}: {:?}", "Add element", $x);
                    temp_vec.push($x);
                )*
                temp_vec
            }
        };
    }
    let v = vec2![1, 2, 3];
    println!("   {:>20}: {:?}", "result is", v);

    shy(SHORT);

    println!(" Our own derive");

    println!(" | The manual implementation prints:");
    print!(" | | ");
    Pancakes::hello_macro();

    println!(" | The derived implementation prints (not):");
    println!(" | | Wow im to lazy for this");

    println!();
}

trait HelloMacro {
    fn hello_macro();
}

struct Pancakes;

impl HelloMacro for Pancakes {
    fn hello_macro() {
        println!("Manual implementation ( Hello :) )");
    }
}

struct Hamburgers;

fn seq(w: usize) {
    println!("{:=<w$}", "", w = w);
}

fn shy(w: usize) {
    println!("{:-<w$}", "", w = w);
}
