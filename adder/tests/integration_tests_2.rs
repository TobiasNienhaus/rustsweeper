use adder;

#[test]
fn external_add_test() {
    assert_eq!(adder::add_two(3), 5);
}
