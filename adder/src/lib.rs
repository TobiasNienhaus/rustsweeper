pub fn add_two(i: i32) -> i32 {
    add(i, 2)
}

fn add(i: i32, j: i32) -> i32 {
    i + j
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn internal_add_test() {
        assert_eq!(add(2, 2), 4);
    }
}
