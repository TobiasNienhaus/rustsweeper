use std::error::Error;
use std::fs;

pub struct Config {
    pub query: String,
    pub file: String,
    pub case_sensitive: bool,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 3 {
            return Err("Not enough arguments!");
        } else if args.len() < 4 {
            Ok(Config {
                query: args[1].clone(),
                file: args[2].clone(),
                case_sensitive: true,
            })
        } else {
            let sensitive = if args[1] == "insensitive" { false } else { true };
            Ok(Config {
                query: args[2].clone(),
                file: args[3].clone(),
                case_sensitive: sensitive,
            })
        }
    }
}

fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut res = Vec::new();
    for line in contents.lines() {
        if line.contains(query) {
            res.push(line);
        }
    }
    res
}

fn search_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut res = Vec::new();
    let query = query.to_lowercase();
    for line in contents.lines() {
        if line.to_lowercase().contains(query.as_str()) {
            res.push(line);
        }
    }
    res
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.file)?;

    let res = if config.case_sensitive {
        search(config.query.as_str(), contents.as_str())
    } else {
        search_insensitive(config.query.as_str(), contents.as_str())
    };
    
    for result in res {
        println!("{}", result);
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_config() {
        let query = String::from("Some Query");
        let file = String::from("Some File");
        let args = vec![String::from("some exe name"), query.clone(), file.clone()];

        let config = Config::new(&args).unwrap_or_else(|err| {
            panic!("Couldn't read config! Error: {}", err);
        });

        assert_eq!(config.query, query);
        assert_eq!(config.file, file);
    }

    #[test]
    #[should_panic]
    fn failing_config_1_arg() {
        let args = vec![String::from("Some exe name")];

        let _ = Config::new(&args).expect("This should fail.");
    }

    #[test]
    #[should_panic]
    fn failing_config_2_args() {
        let args = vec![String::from("Some exe name"), String::from("Hello world")];

        let _ = Config::new(&args).expect("This should fail.");
    }

    #[test]
    fn test_search_case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct Tape";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn test_search_case_insensitive() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me";

        assert_eq!(
            vec!["Rust:", "Trust me"],
            search_insensitive(query, contents)
        );
    }
}
